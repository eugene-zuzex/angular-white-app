//Init module
(function(){
    "use strict";
    
    angular.module('app').config(config);
    
    config.$inject = ['$routeProvider'];
    
    function config($routeProvider) {
        $routeProvider
            .when('/', {
                controller:'MainController',
                controllerAs: 'vm',
                templateUrl:'app/main/main.tmpl.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();