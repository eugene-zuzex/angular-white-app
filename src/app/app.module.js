//Init module
(function(){
    "use strict";
    
    angular.module('app', ['ngMaterial', 'ngResource', 'ngRoute']);
})();