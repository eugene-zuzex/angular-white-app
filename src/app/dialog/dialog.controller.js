//dialog controller
(function(){
    "use strict";
    
    angular.module('app').controller('DialogController', DialogController);

    DialogController.$inject = ['$mdDialog', 'Content'];

    function DialogController($mdDialog, Content) {
        var checkUrlRegex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
        var vm = this;

        vm.closeDialog = closeDialog;
        vm.item = Content.getItem();
        vm.newSource = newSource;
        vm.saveItem = saveItem;
        vm.itemStates = Content.itemStates;

        //////////////

        //close dialog
        function closeDialog() {
            $mdDialog.hide();
        }

        //add handler for sources list
        function newSource(url) {
            if(!url.match(checkUrlRegex))
            {
                vm.showError = true;
                return null;
            }
            vm.showError = false;
            return {
                url: url,
                rating: vm.item.review.rating,
                tags: []
            };
        }

        //save item
        function saveItem() {
            if(Content.setItem(vm.item))
            {
                $mdDialog.hide();
            }
        }
    }
})();
