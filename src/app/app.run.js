//Run block
(function(){
    "use strict";

    angular.module('app').run(runBlock);

    runBlock.$inject = ['$rootScope', '$location', '$http', '$mdDialog', '$timeout'];

    function runBlock($rootScope, $location, $http, $mdDialog, $timeout){
        $rootScope.basepath = $location.absUrl().replace(/(#|&).*/, '');
        $rootScope.loading = true;
        $rootScope.showAlert = showAlert;

        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $http.defaults.transformResponse.push(function(response, callback, status){
            if (status === 401)
            {
                $rootScope.showAlert('Error', response.message || 'Unauthorized request');
                $timeout(function () {
                    location.href = '/';
                }, 1000);
                return false;
            } else if (status !== 200) {
                $rootScope.showAlert('Error', response.message || 'Internal server error: ' + status);
            }
            return response;
        });
        
        //custom alert
        function showAlert(text, title) {
            $mdDialog.show(
                $mdDialog
                    .alert()
                    .clickOutsideToClose(true)
                    .title(title || 'Error')
                    .textContent(text)
                    .ok('Close')
            );
            return false;
        }
    }
})();