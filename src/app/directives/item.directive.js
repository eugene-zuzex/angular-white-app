(function() {
    "use strict";
    
    angular.module('app')
        .directive('contentItem', ItemDirective);
    
    function ItemDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/item.directive.html',
            scope: {
                item: '='
            },
            controller: ItemController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }
    
    ItemController.$inject = ['Content', 'User'];
    
    function ItemController(Content, User) {
        var vm = this;
        
        vm.isManager = User.isManager();
        vm.itemStates = Content.itemStates;
        vm.openDialog = Content.openDialog;
        vm.removeItem = removeItem;
        vm.stopPropagation = stopPropagation;
        
        //remove item from list
        function removeItem(e, id) {
            vm.stopPropagation(e);
            Content.removeItem(id);
            vm.items = Content.getItems();
        }
        
        //stopPropagation for events
        function stopPropagation(e) {
            e.stopPropagation();
            e.stopImmediatePropagation();
        }
    }
    
})();
