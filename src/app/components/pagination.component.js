// Pagination component
(function(){
    "use strict";
    
    angular.module('app').component('pagination', {
        templateUrl: 'app/components/pagination.html',
        controller: PaginationComponent,
        controllerAs: 'vm'
    });

    PaginationComponent.$inject = ['$scope', 'Content'];

    function PaginationComponent($scope, Content) {
        var vm = this;
        var limit, total;

        vm.init = init;
        vm.changePage = changePage;

        $scope.$on('build.pagination', function(){
            init();
        });

        ////////////

        function init() {
            var p = Content.getPagination();
            limit = p.itemsPerPage;
            total = p.total;
            vm.page = p.currentPage;
            if(limit >= total)
            {
                vm.totalPages = 1;
                return false;
            }
            vm.totalPages = Math.ceil(total/limit);
        }

        function changePage(type){
            var page = parseInt(vm.page);
            if (isNaN(page))
            {
                page = 1;
            }
            if (angular.isDefined(type))
            {
                page += (type === 'next' ? 1 : -1);
            }
            if (page > vm.totalPages)
            {
                page = vm.totalPages;
            }
            if (page < 1)
            {
                page = 1;
            }
            vm.page = page;
            Content.setPagination(page, limit);
        }
    }
})();