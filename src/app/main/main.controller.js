//main controller
(function(){
    "use strict";
    
    angular.module('app').controller('MainController', MainController);

    MainController.$inject = ['$scope', '$q', 'User', 'Content'];

    function MainController($scope, $q, User, Content){
        var vm = this;
        vm.loading = true;
        vm.openDialog = Content.openDialog;
        vm.search = '';
        vm.searchS = searchS;
        init();

        ////////////////
        
        function init() {
            $q.all([User.promise, Content.promise]).then(function(){
                vm.isManager = User.isManager();
                vm.loading = false;
                Content.updateView();
            });
            
            //listen update event
            $scope.$on('list.update', function(){
                vm.items = Content.getItems();
            });
        }

        //set search
        function searchS() {
            Content.setSearch(vm.search);
        }
    }
})();