//Content service
(function(){
    "use strict";
    
    angular.module('app').factory('Content', ContentService);

    ContentService.$inject = ['$rootScope', '$filter', '$mdDialog', 'ContentData', 'User'];

    function ContentService($rootScope, $filter, $mdDialog, ContentData, User){
        var currentPage = 1;
        var itemId;
        var itemStates = {
            todo: 'To do',
            tocomplete: 'To complete',
            done: 'Done'
        };
        var items = ContentData.query();
        var itemsPerPage = 5;
        var searchStr = '';

        var service = {
            getItem: getItem,
            getItems: getItems,
            getItemsCount: getItemsCount,
            getPagination: getPagination,
            itemStates: itemStates,
            openDialog: openDialog,
            promise: promise(),
            removeItem: removeItem,
            selectItem: selectItem,
            setItem: setItem,
            setPagination: setPagination,
            setSearch: setSearch,
            updateView: updateView
        };

        return service;

        //////////

        //filter items 
        function _getItems() {
            if(searchStr.length === 0 )
            {
                return items;
            }
            return $filter('filter')(items, {$: searchStr});
        }

        //get index of item by id
        function _getIndexById(id) {
            for(var i in items)
            {
                if(items[i].process.id === id)
                {
                    return i;
                }
            }
            return false;
        }

        //get item to edit
        function getItem() {
            var item = {};
            if(angular.isUndefined(itemId))
            {
                item = {
                    process: {
                        state: 'tocomplete'
                    },
                    review: {
                        tags: [],
                        rating: 1
                    },
                    place: {
                        address: {}
                    },
                    sources: []
                };
            }
            else
            {
                var index = _getIndexById(itemId);
                item = items[index];
            }
            return item;
        }

        //get list of items
        function getItems(page) {
            if(angular.isDefined(page))
            {
                currentPage = page;
            }
            return _getItems().slice((currentPage-1) * itemsPerPage, currentPage * itemsPerPage);
        }

        //get total amount of filtered items
        function getItemsCount() {
            return _getItems().length;
        }

        //get pagination info
        function getPagination() {
            return {
                currentPage: currentPage,
                itemsPerPage: itemsPerPage,
                total: getItemsCount()
            };
        }
        
        //show the dialog
        function openDialog(id) {
            var template = 'app/dialog/dialog.tmpl.html';
            if(User.isManager() && (!id || !User.isWriter()))
            {
                template = 'app/dialog/m-dialog.tmpl.html';
            }
            selectItem(id);
            $mdDialog.show({
                templateUrl: template,
                clickOutsideToClose: true
            });
        }
        
        //return promise
        function promise() {
            return items.$promise;
        }

        //remove item
        function removeItem(id) {
            var index = _getIndexById(id);
            items.splice(index, 1);
            updateView();
            return true;
        }
        
        //select item
        function selectItem(id) {
            itemId = id;
        }

        //save item
        function setItem(data) {
            if(!User.isManager())
            {
                return false;
            }

            if(data.sources.length === 0)
            {
                alert('You should add at least one source');
                return false;
            }
            if(angular.isUndefined(itemId))
            {
                data.process.added = new Date().getTime();
                data.process.id = (items.length !== 0 ? (items[items.length-1].process.id) : 0) + 1;
                
                items.push(data);
                $rootScope.$broadcast('build.pagination');
            }
            else
            {
                var index = _getIndexById(itemId);
                items[index] = data;
            }
            $rootScope.$broadcast('list.update');
            return true;
        }

        //set pagination
        function setPagination(page, limit) {
            currentPage = parseInt(page);
            itemsPerPage = parseInt(limit);
            updateView();
        }

        //set search query
        function setSearch(search){
            searchStr = search.toLowerCase();
            updateView();
            return search;
        }

        //update html view
        function updateView() {
            $rootScope.$broadcast('build.pagination');
            $rootScope.$broadcast('list.update');
        }
    }
    
})();