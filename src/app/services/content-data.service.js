//Content service
(function(){
    "use strict";
    
    angular.module('app').factory('ContentData', ContentDataService);

    ContentDataService.$inject = ['$resource', '$rootScope'];

    function ContentDataService($resource, $rootScope){
        //get items from server
        return $resource($rootScope.basepath + 'data/reviews-data.json');
    }
    
})();