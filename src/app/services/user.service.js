//User service
(function(){
    "use strict";
    
    angular.module('app').factory('User', UserService);

    UserService.$inject = ['$location', 'UserData'];

    function UserService($location, UserData) {
        var user = UserData.get();
        var service = {
            isManager: isManager,
            isWriter: isWriter,
            me: me,
            promise: promise()
        };

        return service;
        
        function isManager() {
            return !!$location.search().manager;
        }

        function isWriter() {
            return !!$location.search().writer;
        }

        function me() {
            return user;
        }
        
        //get user from server
        function promise() {
            return user.$promise;
        }
    }
})();