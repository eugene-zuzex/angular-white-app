//User service
(function(){
    "use strict";
    
    angular.module('app').factory('UserData', UserDataService);

    UserDataService.$inject = ['$resource', '$rootScope'];

    function UserDataService($resource, $rootScope) {
        return $resource($rootScope.basepath + 'data/user.json');
    }
})();