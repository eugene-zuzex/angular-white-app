(function() {
    "use strict";
    
    angular.module('app')
        .filter('getDate', GetDateFilter);

    function GetDateFilter() {
        //format number
        function dubleChar(num) {
            return num < 10 ? '0' + num : num;
        }
        
        return function(date){
            var d = new Date(date);
            return dubleChar(d.getMonth()+1) + '/' + dubleChar(d.getDate()) + '/' + d.getFullYear() + ' ' + dubleChar(d.getHours()) + ':' + dubleChar(d.getMinutes());
        };
    }
    
})();