(function() {
    "use strict";
    
    angular.module('app')
        .filter('getDomain', GetDomainFilter);

    function GetDomainFilter() {
        return function(url) {
            var domain;
            if (url.indexOf("://") > -1) {
                domain = url.split('/')[2];
            }
            else {
                domain = url.split('/')[0];
            }

            domain = domain.split(':')[0];
            return domain;
        };
    }
    
})();