(function() {
    "use strict";
    
    angular.module('app').filter('getRateWidth', GetRateWidthFilter);

    function GetRateWidthFilter() {
        //draw rate
        return function(rate) {
            return {width: Math.floor(rate/5*100) + '%'};
        };
    }
    
})();