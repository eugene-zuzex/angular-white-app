'use strict';

var gulp = require('gulp'),
    S3 = require('gulp-s3-upload'),
    fs = require('fs');

gulp.task('upload-s3', function(){
    var home = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
    var config = JSON.parse(fs.readFileSync(home + '/.aws/credentials.json'));
    config.region = 'eu-central-1';
    var s3 = S3(config);
    console.log(111);
    
    gulp.src(['./dist/**/*'])
        .pipe(s3({
            Bucket: 'angular-dev.eugene-web.ru', 
            ACL:    'public-read'       //  Needs to be user-defined
        }))
    ;
});